import numpy as np


def frangi(l1, l2, a=0.5, b=15, ski=False):
    # Input Lamda1 and Lamda2 (eigenvalues), a and b with default 0.5 and 15
    # Boolean value that signifies if skimage function was used to compute Hessian
    # Returns Frangi filtered image

    l2[l2 == 0.] = 1**-43
    Rb = l1/l2
    S = np.sqrt(l1**2+l2**2)

    F = (np.exp(-(Rb**2)/(2*(a**2)))*(np.ones(S.shape)-np.exp(-(S**2)/(2*(b**2)))))

    F[l2 <= 0] = 0

    # only for Hessian with skimage
    if ski:
        F[l2 > 0.001] = 0

    return F
