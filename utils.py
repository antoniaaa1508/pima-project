from locale import normalize
import numpy as np
import matplotlib.pyplot as plt


def imagePad(I, h):
    # Pads image I with zeros to have size of image h
    # Returns padded image

    sizeW = h.shape[0]-I.shape[0]
    sizeH = h.shape[1]-I.shape[1]
    padded_Ima = np.zeros((I.shape[0]+sizeW, I.shape[1]+sizeH))
    for ix, iy in np.ndindex(I.shape):
        padded_Ima[ix+sizeW//2, iy+sizeH//2] = I[ix, iy]
    return padded_Ima


def normalize(I):
    norm = np.zeros(I.shape)
    maxi = np.amax(I)
    mini = np.amin(I)
    norm = (I-mini)/maxi

    return norm


def imshow(I, title=None, size=500, axis=False):
    plt.figure(figsize=(size//80, size//80))
    plt.gray()
    plt.imshow(I)
    if not axis:
        plt.axis('off')
    if title:
        plt.title(title)
    plt.show()
