import numpy as np


def jerman(l2, t=1.0):
    # Input Lamda2 (eigenvalue), t with default 1.0
    # Returns Jerman filtered image

    lambda_p = l2.copy()
    lambda_p[(0 < l2) * (l2 <= t * np.amax(l2))] = t * np.amax(l2)
    lambda_p[l2 <= 0] = 0
    l2[l2 == 0] = 1**-43
    F = l2**2*(lambda_p-l2)*((np.full((l2.shape), 3)/(l2+lambda_p))**3)

    F[(l2 <= 0) + (lambda_p <= 0)] = 0
    F[(l2 >= lambda_p/2) * (lambda_p/2 > 0)] = 1

    return F
